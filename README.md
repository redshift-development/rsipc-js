# RedShift IPC for JS

RSIPC client for JavaScript environments

Currently focused on Electron apps

[Feature Document](https://gitlab.com/redshift-development/rsipc-js/-/blob/main/design/features.md)
