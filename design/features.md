# TODO:

## Environment support

For now the focus is on Electron apps. This project won't be too concerned with providing an easy API for other types of Node.js applications, but it should separate the electron elements from the IPC elements where possible to allow for a more general purpose library in the future.

## HTTP Verb support

- POST (default)
- GET
- PUT
- DELETE

## Userspace API

- Simple `fetch()` style API (stripped down features at launch)
- The ServiceBridge class will provide a `request()` method
- Requests will take a `path`, `verb` (optional), `config` (optional), and `data` (optional)
- The request method will return a `Response` Promise
- Data content will not be validated by the library for now

## Wiring Goals

- The IPC client has to handle configuration in multiple zones when running in Electron
  - The first part will be the IPC bridge
    - Used to connect to a named pipe and run an HTTP request
    - Will be given a Request object from userspace
    - Will return a Promise for a Response object
  - The second part will run as a preload
    - This part should generate an IPC client to communicate with the main IPC bridge
    - The IPC client will be bound to the window using `contextBridge.exposeInMainWorld`
  - The third and final part will be calling requests on the client from the browser side. This uses the exposed client to access the userspace API
