/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

import { request } from 'http'
import { Buffer } from 'buffer'

interface IpcClient {
  send(channel: 'rsipc-emit', method: string, data: string): void
}

interface IpcEvent extends Event {
  sender: IpcClient
}

interface IpcService {
  handle(
    channel: 'rsipc-request',
    listener: (event: IpcEvent, name: string, method: string, data: string) => Promise<string>,
  ): void
  handle(channel: 'rsipc-emit', listener: (event: IpcEvent, method: string, data: string) => unknown): void
}

export const NewServiceBridge = (ipc: IpcService): void => {
  ipc.handle('rsipc-request', (_, name, method, data): Promise<string> => {
    return new Promise<string>((resolve, reject) => {
      const opts = {
        socketPath: `\\\\.\\pipe\\${name}`,
        path: `${method}`,
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(data),
        },
      }
      const req = request(opts, (res) => {
        // Reconstruct response from chunks
        const response: Uint8Array[] = []
        res.on('data', (chunk) => {
          response.push(chunk)
        })
        res.on('end', () => {
          resolve(response.join(''))
        })
      })
      req.on('error', (err) => {
        reject(err)
      })
      req.write(data)
      req.end()
    })
  })
}
