/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

interface IpcClient {
  invoke(channel: 'rsipc-request', name: string, method: string, data: string): Promise<string>
}

export interface IServiceClient {
  request<T>(method: string, data: T): Promise<string>
}

export const NewServiceClient = (name: string, ipc: IpcClient): IServiceClient => {
  const request = async <T>(method: string, data: T) => {
    try {
      const resData = await ipc.invoke('rsipc-request', name, method, JSON.stringify(data))
      // should not return, should parse then look for data or error key, return or throw accordingly
      return resData as string
    } catch (e) {
      // Something went wrong during the request or JSON parsing. Throw the error
      throw new Error(e)
    }
  }
  return { request }
}
